package ru.itis.enums;

public enum DoctorState {
    CREATED,
    DELETED
}

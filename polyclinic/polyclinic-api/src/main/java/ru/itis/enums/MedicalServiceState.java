package ru.itis.enums;

public enum MedicalServiceState {
    ACTIVE,
    DELETED
}

package ru.itis.enums;

public enum AccountState {
    ALLOWED,
    NOT_ALLOWED,
    DELETED
}

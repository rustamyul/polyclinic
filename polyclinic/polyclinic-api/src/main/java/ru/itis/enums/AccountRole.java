package ru.itis.enums;

public enum AccountRole {
    ADMIN,
    PATIENT,
    DOCTOR
}

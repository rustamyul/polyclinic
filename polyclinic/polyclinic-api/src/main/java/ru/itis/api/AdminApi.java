package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.MedicalServiceResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/admin")
public interface AdminApi {

    @Operation(summary = "Обновление информации о докторе")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленная информация о докторе",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DoctorResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "User not found", content = @Content)
    })
    @PutMapping("/doctor/{id}")
    @ResponseStatus(OK)
    DoctorResponse editDoctorInformation(@PathVariable("id") UUID id,
                                         @RequestBody EditDoctorInfoRequest request);



    @Operation(summary = "Удаление аккаунта доктора")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Аккаунт доктора успешно удален",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Doctor not found", content = @Content)})
    @DeleteMapping("/doctor/{id}")
    @ResponseStatus(OK)
    DeleteResponse deleteDoctor(@PathVariable("id") UUID id);


    @Operation(summary = "Обновление информации о категории")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Категория",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CategoryResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Category not found", content = @Content)
    })
    @PutMapping("/category/{id}")
    @ResponseStatus(OK)
    CategoryResponse editCategory(@PathVariable("id") UUID id,
                                  @RequestBody EditCategoryRequest request);



    @Operation(summary = "Удаление категории мед. услуг")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Категория успешно удалена",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Category not found", content = @Content)})
    @DeleteMapping("/category/{id}")
    @ResponseStatus(OK)
    DeleteResponse deleteCategory(@PathVariable("id") UUID id);


    @Operation(summary = "Обновление информации о медецинской услуге")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Медицинская услуга",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MedicalServiceResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Medical service not found", content = @Content)
    })
    @PutMapping("/medical_service/{id}")
    @ResponseStatus(OK)
    MedicalServiceResponse editMedicalService(@PathVariable("id") UUID id, @RequestBody EditMedicalServiceRequest request);



    @Operation(summary = "Удаление медицинской услуги")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Медицинская услуга успешно удалена",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Medical Service not found", content = @Content)})
    @DeleteMapping("/medical_service/{id}")
    @ResponseStatus(OK)
    DeleteResponse deleteMedicalService(@PathVariable("id") UUID id);
}

package ru.itis.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.OpenAPI31;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.repository.query.Param;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreateAppointmentRequest;
import ru.itis.dto.request.EditAppointmentRequest;
import ru.itis.dto.response.*;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("polyclinic/api/appointment")
public interface AppointmentApi<PRINCIPAL extends UserDetails, Entity> {

    @Operation(summary = "Создание приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Прием",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = DoctorResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @ResponseStatus(CREATED)
    PatientAppointmentResponse createAppointment( @AuthenticationPrincipal PRINCIPAL principal,
                                                 @RequestBody CreateAppointmentRequest createAppointmentRequest);


    @Operation(summary = "Добавление файла к приему")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Информация о файле",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FileInfoResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping("/{id}/{file_id}")
    @ResponseStatus(OK)
    FileInfoResponse setFileForAppointment(@AuthenticationPrincipal PRINCIPAL principal,
                                           @PathVariable("id")UUID id,
                                           @PathVariable("file_id") UUID fileId);


    @Operation(summary = "Обновление или добавление информации о приеме доктором")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Информация о приеме",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DoctorAppointmentResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Appointment not found", content = @Content)
    })
    @PutMapping("/{id}")
    @ResponseStatus(OK)
    DoctorAppointmentResponse editAppointmentInfo(@AuthenticationPrincipal PRINCIPAL principal,
                                                  @PathVariable("id")UUID id,
                                                  @RequestBody EditAppointmentRequest request);

    @Operation(summary = "Отмена приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "204", description = "Прием успешно отменен",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DeleteResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Appointment not found", content = @Content)})
    @DeleteMapping("/{id}")
    @ResponseStatus(OK)
    DeleteResponse cancelAppointment(@AuthenticationPrincipal PRINCIPAL principal,
                                     @PathVariable("id")UUID id);


    @Operation(summary = "Получение списка приемов пациента с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список приемов",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @GetMapping("/patient")
    @ResponseStatus(OK)
    PageOfListResponse<PatientAppointmentResponse> getPatientAppointments(@AuthenticationPrincipal UserDetails userDetails,
                                                                          @RequestParam("page") int page,
                                                                          @RequestParam("size") int size,
                                                                          Specification<Entity> spec);


    @Operation(summary = "Получение списка приемов доктора с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список приемов",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @GetMapping("/doctor")
    @ResponseStatus(OK)
    PageOfListResponse<DoctorAppointmentResponse> getDoctorAppointments(@AuthenticationPrincipal UserDetails userDetails,
                                                                        @RequestParam("page") int page,
                                                                        @RequestParam("size") int size,
                                                                                Specification<Entity> spec);
}

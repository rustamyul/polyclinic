package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.CreateDoctorRequest;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/doctor")
public interface DoctorApi<PRINCIPAL extends UserDetails, Entity> {

    @Operation(summary = "Получение доктора через Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Доктор",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = DoctorResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Doctor not found", content = @Content)})
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    DoctorResponse getDoctorById(@PathVariable("id") UUID id);


    @Operation(summary = "Получение списка докторов с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список докторов",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @GetMapping
    @ResponseStatus(OK)
    PageOfListResponse<DoctorResponse> getDoctors(@RequestParam("page") int page,
                                                  @RequestParam("size") int size,
                                                  Specification<Entity> spec);

    @Operation(summary = "Дополнение информации о докторе")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Доктор",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = DoctorResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "File not found", content = @Content)
    })
    @PostMapping
    @ResponseStatus(CREATED)
    DoctorResponse createDoctor(@AuthenticationPrincipal PRINCIPAL principal,
                                @RequestBody CreateDoctorRequest request);

    @Operation(summary = "Фотография доктора")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "201", description = "Информация о файле",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = FileInfoResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping("/{photo_id}")
    @ResponseStatus(OK)
    FileInfoResponse setDoctorPhoto(@AuthenticationPrincipal PRINCIPAL principal,
                                    @PathVariable("photo_id") UUID photoId);


}

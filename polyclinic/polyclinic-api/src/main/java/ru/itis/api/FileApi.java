package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.FileInfoResponse;

import java.util.UUID;

@RequestMapping("/api/file")
public interface FileApi {

    @Operation(summary = "Get file info through id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "File info",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FileInfoResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "File not found", content = @Content)
    })
    @GetMapping(value = "/{id}/info")
    @ResponseStatus(HttpStatus.OK)
    FileInfoResponse getFileInfo(@PathVariable("id")UUID uuid);

    @Operation(summary = "Get file content through id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "File content",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = byte[].class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "File not found", content = @Content)
    })
    @GetMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    ResponseEntity<byte[]> getFile(@PathVariable("id")UUID uuid);

    @Operation(summary = "Save file")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "File saved",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = FileInfoResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)
    })
    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    FileInfoResponse saveFile(@RequestParam("file") MultipartFile multipartFile);

}

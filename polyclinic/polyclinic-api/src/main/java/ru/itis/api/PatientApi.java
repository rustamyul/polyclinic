package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;

import java.time.Instant;
import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/patient")
public interface PatientApi<PRINCIPAL extends UserDetails> {

    @Operation(summary = "Поменять доктора")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленная информация о приеме",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PatientAppointmentResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Appointment not found", content = @Content)
    })
    @PutMapping("/{id}/doctor/{doctor_id}")
    @ResponseStatus(OK)
    PatientAppointmentResponse changeDoctor(@AuthenticationPrincipal PRINCIPAL principal,
                                            @PathVariable("id") UUID appointmentId,
                                            @PathVariable("doctor_id")UUID doctorId);


    @Operation(summary = "Поменять время приема")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Обновленная информация о приеме",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PatientAppointmentResponse.class)) }),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Appointment not found", content = @Content)
    })
    @PutMapping("/{id}/time/{time}")
    @ResponseStatus(OK)
    PatientAppointmentResponse changeTime(@AuthenticationPrincipal PRINCIPAL principal,
                                          @PathVariable("id") UUID appointmentId,
                                          @PathVariable("time") Instant time);


}

package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.Getter;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.PageOfListResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/category")
public interface CategoryApi{

    @Operation(summary = "Получение списка категорий с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список категорий",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Category not found", content = @Content)})
    @GetMapping
    @ResponseStatus(OK)
    PageOfListResponse<CategoryResponse> getCategoryList(@RequestParam("page") int page,
                                                         @RequestParam("size") int size);



    @Operation(summary = "Получение категории через Id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Доктор",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = CategoryResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Category not found", content = @Content)})
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    CategoryResponse getCategoryById(@PathVariable("id") UUID id);

}

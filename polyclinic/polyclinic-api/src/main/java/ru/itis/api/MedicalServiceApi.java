package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.*;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.dto.response.PageOfListResponse;

import java.util.UUID;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/medical_service")
public interface MedicalServiceApi<Entity> {


    @Operation(summary = "Получение медицинской услуги")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Медицинская услуга",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MedicalServiceResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Medical Service not found", content = @Content)})
    @GetMapping("/{id}")
    @ResponseStatus(OK)
    MedicalServiceResponse getMedicalService(@PathVariable("id") UUID id);



    @Operation(summary = "Получение списка медицинских услуг с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список медицинских услуг",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content),
            @ApiResponse(responseCode = "404", description = "Medical Service not found", content = @Content)})
    @GetMapping
    @ResponseStatus(OK)
    PageOfListResponse<MedicalServiceResponse> getMedicalServiceList(@RequestParam("page") int page,
                                                                     @RequestParam("size") int size,
                                                                     Specification<Entity> spec);

}

package ru.itis.api;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import ru.itis.dto.response.appointment.FreeAppointmentResponse;
import ru.itis.dto.response.PageOfListResponse;

import static org.springframework.http.HttpStatus.OK;

@RequestMapping("/polyclinic/api/free_appointment")
public interface FreeAppointmentApi<Entity> {

    @Operation(summary = "Получение списка свободных приемов с пагинацией")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Список свободных приемов",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = PageOfListResponse.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid request", content = @Content)})
    @GetMapping
    @ResponseStatus(OK)
    PageOfListResponse<FreeAppointmentResponse> getFreeAppointments(int page, int size,
                                                                    Specification<Entity> spec);
}

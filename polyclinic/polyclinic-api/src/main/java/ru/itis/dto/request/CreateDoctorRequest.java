package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для указания информации о докторе")
public class CreateDoctorRequest {

    @Schema(description = "ФИО доктора", example = "Иванов Иван Иванович")
    @JsonProperty("full_name")
    @NotBlank(message = "field \"full_name\" cannot be empty")
    private String fullName;

    @Schema(description = "Стаж доктора", example = "4г.")
    @JsonProperty("experience")
    @NotBlank(message = "field \"experience\" cannot be empty")
    private String experience;

    @Schema(description = "Образование доктора", example = "Высшее-КФУ")
    @JsonProperty("education")
    @NotBlank(message = "Education cannot be empty")
    private String education;
}

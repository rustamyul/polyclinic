package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Информация о файле")
public class FileInfoResponse {

    @Schema(description = "id файла", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("file_id")
    private UUID fileId;

    @Schema(description = "Название файла", example = "file1.png")
    @JsonProperty("file_name")
    private String filename;

    @Schema(description = "Размер файла", example = "1111")
    @JsonProperty("file_size")
    private Long fileSize;

    @Schema(description = "Content type", example = "image/jpeg")
    @JsonProperty("content_type")
    private String contentType;
}




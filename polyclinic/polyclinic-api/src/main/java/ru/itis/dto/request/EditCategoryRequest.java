package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для редактирования категории")
public class EditCategoryRequest {

    @Schema(description = "Название категории", example = "МРТ")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание категории", example = "МРТ - ...")
    @JsonProperty("description")
    private String description;
}

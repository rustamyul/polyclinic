package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Пара токенов")
public class TokenCoupleResponse {

    @Schema(description = "Access token", example = "Bearer eyJhbGciOiJIUzUxMiJ9.eyJST0xFIjoiRE9DVE9SIiwic3ViIjoicnVzdGFteXVsNkBnbWFpbC5jb20iLCJle" +
            "HAiOjE2NzA4MzYxMzAsImlhdCI6MTY3MDgzNjA0M30.8r4LdxWedex1-6GXN_qrWAfZRB4y0G7AposRWEjidjmzIb1hHHt5t6GXxYUiakFimDKmrAp6nz9dszaghH5iDw")
    @JsonProperty("access_token")
    private String accessToken;

    @Schema(description = "Refresh token", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("refresh_token")
    private UUID refreshToken;

    @Schema(description = "Дата и время истечения Access токена", example = "2022-12-12T09:08:50.000+00:00")
    @JsonProperty("expire_date")
    private Instant expireDate;
}

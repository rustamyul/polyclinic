package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для обновления или добавления информации о приеме")
public class EditAppointmentRequest {

    @Schema(description = "Рекомендации для пациента", example = "")
    @JsonProperty("recommendation")
    private String recommendation;

    @Schema(description = "Информация о приеме для доктора", example = "")
    @JsonProperty("description")
    private String description;

    @Schema(description = "История болезней", example = "")
    @JsonProperty("medical_history")
    private String medicalHistory;

    @Schema(description = "Ссылки на файлы")
    @JsonProperty("file_links")
    private String fileLinks;

    @Schema(description = "Информация о приеме")
    @JsonProperty("comment")
    private String comment;

    @Schema(description = "id файла")
    @JsonProperty("file_id")
    private UUID fileId;

}

package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.time.Instant;
import java.util.UUID;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для создания приема к доктору")
public class CreateAppointmentRequest {

    @Schema(description = "id мед. услуги", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("medical_service_id")
    @NotBlank(message = "field \"medical_service_id\" cannot be empty")
    private UUID medicalServiceId;

    @Schema(description = "id доктора", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("doctor_id")
    @NotBlank(message = "field \"doctor_id\" id cannot be empty")
    private UUID doctorId;

    @Schema(description = "Время начала приема", example = "2022-12-22 08:30:00.000000")
    @JsonProperty("start_appointment")
    @NotBlank(message = "field \"start_appointment\" cannot be empty")
    private Instant startAppointment;

    @Schema(description = "Комментарии к приему", example = "Денег нет")
    @JsonProperty("comment")
    private String comment;

}

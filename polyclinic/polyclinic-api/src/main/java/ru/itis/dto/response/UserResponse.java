package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

import java.util.UUID;

@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Информация о пользователе")
public class UserResponse {

    @Schema(description = "id пользователя", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    private String email;

    @Schema(description = "Phone", example = "+71234567890")
    @JsonProperty("phone")
    private String phone;
}

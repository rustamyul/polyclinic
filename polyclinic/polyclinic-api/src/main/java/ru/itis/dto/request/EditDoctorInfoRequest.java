package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для редактирования профиля доктора")
public class EditDoctorInfoRequest {

    @Schema(description = "ФИО доктора", example = "Иванов Иван Иванович")
    @JsonProperty("full_name")
    private String fullName;

    @Schema(description = "Стаж доктора", example = "4г.")
    @JsonProperty("experience")
    private String experience;

    @Schema(description = "Образование доктора", example = "Высшее-КФУ")
    @JsonProperty("education")
    private String education;
}

package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Медицинская услуга")
public class MedicalServiceResponse {

    @Schema(description = "id мед. услуги", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Название мед. услуги", example = "МРТ колена")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Стоимость мед. услуги", example = "1990")
    @JsonProperty("cost")
    private Integer cost;

    @Schema(description = "Описание мед. услуги", example = "Мрт колена - ...")
    @JsonProperty("description")
    private String description;

    @JsonProperty("category")
    private CategoryResponse category;
}

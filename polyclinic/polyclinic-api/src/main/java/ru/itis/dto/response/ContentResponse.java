package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Data
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "File's content")
public class ContentResponse extends FileInfoResponse {

    @Schema(description = "byte array")
    @JsonProperty("file")
    private byte[] file;
}

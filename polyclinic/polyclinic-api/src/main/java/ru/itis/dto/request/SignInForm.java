package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для входа в систему")
public class SignInForm {

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    @NotBlank(message = "field \"email\" cannot be empty")
    private String email;

    @Schema(description = "Пароль пользователя", example = "qwerty000")
    @JsonProperty("password")
    @NotBlank(message = "field \"password\" cannot be empty")
    private String password;
}

package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Список с пагинацией")
public class PageOfListResponse<T> {

    @Schema(description = "Список")
    @JsonProperty("list")
    private List<T> list;

    @Schema(description = "Общее кол-во страниц", example = "1")
    @JsonProperty("total_page")
    private int totalPage;

    @Schema(description = "Размер страницы", example = "1")
    @JsonProperty("size")
    private int size;
}


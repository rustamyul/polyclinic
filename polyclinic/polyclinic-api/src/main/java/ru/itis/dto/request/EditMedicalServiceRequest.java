package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для редактирования мед. услуги")
public class EditMedicalServiceRequest {

    @Schema(description = "Название мед. услуги", example = "МРТ колена")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Стоимость мед. услуги", example = "1990")
    @JsonProperty("cost")
    private Integer cost;

    @Schema(description = "Описание мед. услуги", example = "Мрт колена - ...")
    @JsonProperty("description")
    private String description;
}

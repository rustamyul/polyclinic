package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Категории медицинских услуг")
public class CategoryResponse {

    @Schema(description = "id категории", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Название категории", example = "Компьюторная томография")
    @JsonProperty("title")
    private String title;

    @Schema(description = "Описание категории", example = "Метод неразрушающего послойного исследования")
    @JsonProperty("description")
    private String description;
}

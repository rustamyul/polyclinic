package ru.itis.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.itis.enums.AccountRole;
import ru.itis.validation.ValidPassword;
import ru.itis.validation.ValidSameFields;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Getter
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Форма для регистрации пользователя")
@ValidSameFields(names = {"password", "repeatPassword"}, message = "Passwords do not match")
public class CreateUserRequest {

    @Schema(description = "Почта пользователя", example = "ivanov@mail.ru")
    @JsonProperty("email")
    @NotBlank(message = "field \"email\" cannot be empty")
    @Email(regexp = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$",
            message = "Email not valid")
    private String email;

    @Schema(description = "Новый пароль пользователя", example = "qwerty000")
    @JsonProperty("password")
    @ValidPassword
    @NotBlank(message = "field \"password\" cannot be empty")
    private String password;

    @Schema(description = "Повтор нового пароля", example = "qwerty000")
    @JsonProperty("repeat_password")
    private String repeatPassword;

    @Schema(description = "Phone", example = "+71234567890")
    @JsonProperty("phone")
    private String phone;

    @Schema(description = "Роль пользователя", example = "PATIENT")
    @JsonProperty("role")
    private AccountRole role;
}

package ru.itis.dto.response.appointment;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.dto.response.DoctorResponse;

import java.time.Instant;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FreeAppointmentResponse {

    @Schema(description = "id приема", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Время начала приема", example = "2022-12-22 08:30:00.000000")
    @JsonProperty("start_time")
    private Instant startTime;

    @JsonProperty("doctor")
    private DoctorResponse doctor;
}

package ru.itis.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Schema(description = "Доктор")
public class DoctorResponse {

    @Schema(description = "id доктора", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "ФИО доктора", example = "Иванов Иван Иванович")
    @JsonProperty("full_name")
    private String fullName;

    @Schema(description = "Стаж доктора", example = "4г.")
    @JsonProperty("experience")
    private String experience;

    @Schema(description = "Образование доктора", example = "Высшее-КФУ")
    @JsonProperty("education")
    private String education;

    @JsonProperty("photo")
    private FileInfoResponse photo;
}

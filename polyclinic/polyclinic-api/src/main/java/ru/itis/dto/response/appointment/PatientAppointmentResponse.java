package ru.itis.dto.response.appointment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.FileInfoResponse;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Ифнормация о приеме для пациента")
public class PatientAppointmentResponse {

    @Schema(description = "id приема", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Время начала приема", example = "2022-12-22 08:30:00.000000")
    @JsonProperty("start_time")
    private Instant startTime;

    @Schema(description = "Комментарии к приему", example = "Денег нет")
    @JsonProperty("comment")
    private String comment;

    @JsonProperty("doctor")
    private DoctorResponse doctor;

    @JsonProperty("files")
    private List<FileInfoResponse> files;
}


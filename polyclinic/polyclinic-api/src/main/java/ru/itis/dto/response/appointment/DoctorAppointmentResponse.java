package ru.itis.dto.response.appointment;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.itis.dto.response.FileInfoResponse;

import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Информация о приеме для доктора")
public class DoctorAppointmentResponse {

    @Schema(description = "id приема", example = "c850e74b-3b5e-4a89-b6dd-54cf45f45b5n")
    @JsonProperty("id")
    private UUID id;

    @Schema(description = "Время начала приема", example = "2022-12-22 08:30:00.000000")
    @JsonProperty("start_time")
    private Instant startTime;

    @Schema(description = "Комментарии к приему", example = "Денег нет")
    @JsonProperty("comment")
    private String comment;

    @Schema(description = "Рекомендации для пациента", example = "Не есть")
    @JsonProperty("recommendation")
    private String recommendation;

    @Schema(description = "Информация о приеме для доктора", example = "Пациент нервный")
    @JsonProperty("description")
    private String description;

    @Schema(description = "История болезней", example = "1. 12.10.21 ...")
    @JsonProperty("medical_history")
    private String medicalHistory;

    @Schema(description = "Ссылки на файлы")
    @JsonProperty("file_links")
    private String fileLinks;

    @JsonProperty("files")
    private List<FileInfoResponse> files;
}

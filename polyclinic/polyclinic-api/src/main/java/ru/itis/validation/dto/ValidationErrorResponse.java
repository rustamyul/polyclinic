package ru.itis.validation.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
@Schema(description = "Информация об ошибки при валидации")
public class ValidationErrorResponse {

    @Schema(description = "field", example = "title")
    private String field;

    @Schema(description = "object", example = "user")
    private String object;

    @Schema(description = "error message", example = "Password is incorrect")
    private String message;
}

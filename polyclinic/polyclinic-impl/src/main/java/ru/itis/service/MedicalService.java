package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.MedicalServiceEntity;

import java.util.UUID;

public interface MedicalService {
    PageOfListResponse<MedicalServiceResponse> getMedicalServiceList(int page, int size, Specification<MedicalServiceEntity> spec);

    MedicalServiceResponse getMedicalService(UUID id);
}

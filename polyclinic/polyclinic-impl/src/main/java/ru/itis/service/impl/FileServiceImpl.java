package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.ContentResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.enums.FileState;
import ru.itis.exception.FileNotFoundException;
import ru.itis.model.FileEntity;
import ru.itis.repository.FileRepository;
import ru.itis.service.FileOperationService;
import ru.itis.service.FileService;
import ru.itis.utils.mapstruct.FileMapper;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileServiceImpl implements FileService {

    private final FileOperationService fileOperationService;
    private final FileRepository fileRepository;
    private final FileMapper fileMapper;

    @Override
    public FileInfoResponse getFileInfo(UUID uuid) {
        log.info("Get file-info through id, id - {}", uuid);
        return fileMapper.toFileResponse(getFileEntity(uuid));
    }

    @Override
    public ContentResponse getFile(UUID uuid) {
        log.info("Get file through id, id - {}", uuid);
        FileEntity file = getFileEntity(uuid);
        ContentResponse contentResponse = fileMapper.toContentResponse(file);
        contentResponse.setFile(fileOperationService.getFile(file.getFileId()));
        return contentResponse;
    }

    @Transactional
    @Override
    public FileInfoResponse saveFile(MultipartFile multipartFile) {
        log.info("Save new file, file-name - {}", multipartFile.getOriginalFilename());
        return fileMapper.toFileResponse(saveStorageFile(multipartFile));
    }

    @Transactional
    public FileEntity saveStorageFile(MultipartFile multipartFile){
        return fileRepository.save(FileEntity.builder()
                .filename(multipartFile.getOriginalFilename())
                .contentType(multipartFile.getContentType())
                .size(multipartFile.getSize())
                .fileId(fileOperationService.saveFile(multipartFile))
                .state(FileState.NOT_DELETED)
                .build());
    }

    @Override
    public FileEntity getFileEntity(UUID uuid) {
        log.info("Get file info through id - {}", uuid);
        return fileRepository.findById(uuid)
                .orElseThrow(FileNotFoundException::new);
    }

    @Override
    public void deleteFile(FileEntity file){
        file.setState(FileState.DELETED);
    }

    @Transactional
    @Scheduled(cron = "${cron.delete.file.time}")
    public void deleteUnnecessaryFile(){
        log.info("Deleted unnecessary file");
        List<FileEntity> files = fileRepository.findAllByState(FileState.DELETED);
        fileOperationService.deleteFiles(files.stream().map(FileEntity::getFileId).collect(Collectors.toList()));
        fileRepository.deleteInBatch(files);
    }

}

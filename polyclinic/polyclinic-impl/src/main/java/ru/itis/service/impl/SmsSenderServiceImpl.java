package ru.itis.service.impl;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.itis.service.SmsSenderService;


@Slf4j
@Service
@RequiredArgsConstructor
public class SmsSenderServiceImpl implements SmsSenderService {

    @Value("${spring.sms.twilio.number}")
    private String TWILIO_NUMBER;

    @Value("${spring.sms.auth.token}")
    private String AUTH_TOKEN;

    @Value("${spring.sms.account.sid}")
    private String ACCOUNT_SID;

    @Override
    public void sendSms(String to, String text) {
        log.info("Send sms to patient");
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message.creator(
                new PhoneNumber(to),
                new PhoneNumber(TWILIO_NUMBER),
                text)
            .create();
    }
}

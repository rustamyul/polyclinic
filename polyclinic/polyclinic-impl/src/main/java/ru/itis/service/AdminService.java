package ru.itis.service;

import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.MedicalServiceResponse;

import java.util.UUID;

public interface AdminService {

    DoctorResponse editDoctorInformation(UUID id, EditDoctorInfoRequest request);

    DeleteResponse deleteDoctor(UUID id);

    CategoryResponse editCategory(UUID id, EditCategoryRequest request);

    DeleteResponse deleteCategory(UUID id);

    MedicalServiceResponse editMedicalService(UUID id, EditMedicalServiceRequest request);

    DeleteResponse deleteMedicalService(UUID id);
}

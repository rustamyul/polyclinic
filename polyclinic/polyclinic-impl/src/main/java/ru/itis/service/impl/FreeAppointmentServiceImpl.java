package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.appointment.FreeAppointmentResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.FreeAppointmentEntity;
import ru.itis.repository.FreeAppointmentRepository;
import ru.itis.service.FreeAppointmentService;
import ru.itis.utils.mapstruct.FreeAppointmentMapper;

import javax.transaction.Transactional;
import java.time.Instant;

@Slf4j
@Service
@RequiredArgsConstructor
public class FreeAppointmentServiceImpl implements FreeAppointmentService {

    private final FreeAppointmentRepository freeAppointmentRepository;
    private final FreeAppointmentMapper freeAppointmentMapper;

    @Override
    public PageOfListResponse<FreeAppointmentResponse> getFreeAppointments(int page, int size, Specification<FreeAppointmentEntity> spec) {
        log.info("Get free appointments");
        Page<FreeAppointmentEntity> result = freeAppointmentRepository.findAll(spec, PageRequest.of(page, size,  Sort.by("startTime")));

        return PageOfListResponse.<FreeAppointmentResponse>builder()
                .list(freeAppointmentMapper.toFreeAppointmentResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    @Transactional
    @Scheduled(cron = "${cron.delete.free.appointment.time}")
    public void deleteUnnecessaryFile(){
        log.info("Delete expired appointments");
        freeAppointmentRepository.deleteAllByStartTimeBefore(Instant.now());
    }
}

package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.exception.CategoryNotFoundException;
import ru.itis.model.CategoryEntity;
import ru.itis.repository.CategoryRepository;
import ru.itis.service.CategoryService;
import ru.itis.utils.mapstruct.CategoryMapper;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    @Override
    public PageOfListResponse<CategoryResponse> getCategoryList(int page, int size) {
        log.info("Get list of categories");

        Page<CategoryEntity> result = categoryRepository.findAll(PageRequest.of(page, size));
        return PageOfListResponse.<CategoryResponse>builder()
                .list(categoryMapper.toCategoryResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    @Override
    public CategoryResponse getCategoryById(UUID id) {
        log.info("Get category by id, id - {}", id);

        return categoryMapper.toCategoryResponse(findCategoryById(id));
    }

    private CategoryEntity findCategoryById(UUID id) {
        return categoryRepository.findById(id)
                .orElseThrow(CategoryNotFoundException::new);
    }
}

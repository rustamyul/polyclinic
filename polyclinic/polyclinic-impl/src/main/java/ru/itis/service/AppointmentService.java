package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreateAppointmentRequest;
import ru.itis.dto.request.EditAppointmentRequest;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.model.AppointmentEntity;

import java.util.UUID;

public interface AppointmentService {

    PatientAppointmentResponse createAppointment(UserDetails userDetails, CreateAppointmentRequest createAppointmentRequest);

    DeleteResponse cancelAppointment(UserDetails userDetails, UUID id);

    FileInfoResponse setFileForAppointment(UserDetails userDetails, UUID id, UUID fileId);

    DoctorAppointmentResponse editAppointmentInfo(UserDetails userDetails, UUID id, EditAppointmentRequest request);

    PageOfListResponse<DoctorAppointmentResponse> getDoctorAppointments(UserDetails userDetails, int page, int size, Specification<AppointmentEntity> spec);

    PageOfListResponse<PatientAppointmentResponse> getPatientAppointments(UserDetails userDetails, int page, int size, Specification<AppointmentEntity> spec);
}

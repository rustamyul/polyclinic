package ru.itis.service;

public interface SmsSenderService {

    void sendSms(String to, String text);
}

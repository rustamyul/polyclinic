package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.SignInForm;
import ru.itis.dto.response.CreateUserResponse;
import ru.itis.enums.AccountState;
import ru.itis.exception.user.PasswordIsIncorrectException;
import ru.itis.exception.user.UserWithSuchEmailExist;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.UserEntity;
import ru.itis.repository.UserRepository;
import ru.itis.service.UserService;
import ru.itis.utils.mapstruct.UserMapper;

@Slf4j
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserMapper userMapper;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserEntity getUserBySubject(String email) {
        log.info("Get user through user-email, user-email - {}", email);
        return userRepository.findByEmail(email)
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public UserEntity login(SignInForm signInForm) {
        log.info("Authentication user, user-email - {}", signInForm.getEmail());
        UserEntity user = getUserBySubject(signInForm.getEmail());

        if (!passwordEncoder.matches(signInForm.getPassword(), user.getHashPassword())){
            throw new UserNotFoundException();
        }
        return user;
    }

    @Override
    public CreateUserResponse createUser(CreateUserRequest userRequest) {
        log.info("User registration, user-email - {}", userRequest.getEmail());
        if (userRepository.existsByEmail(userRequest.getEmail())) {
            throw new UserWithSuchEmailExist();
        }

        String password = userRequest.getPassword();
        if (!password.equals(userRequest.getRepeatPassword())) {
            throw new PasswordIsIncorrectException("Passwords don't match");
        }

        UserEntity newUser = userRepository.save(
                UserEntity.builder()
                        .email(userRequest.getEmail())
                        .hashPassword(passwordEncoder.encode(password))
                        .role(userRequest.getRole())
                        .state(AccountState.ALLOWED)
                        .build());

        return userMapper.toCreateUserRequest(newUser);
    }
}

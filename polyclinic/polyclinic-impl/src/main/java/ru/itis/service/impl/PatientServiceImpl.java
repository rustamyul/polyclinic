package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.exception.AppointmentNotFoundException;
import ru.itis.exception.DoctorNotFoundException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.AppointmentEntity;
import ru.itis.model.DoctorEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.AppointmentRepository;
import ru.itis.repository.DoctorRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.PatientService;
import ru.itis.utils.mapstruct.AppointmentMapper;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class PatientServiceImpl implements PatientService {


    private final AppointmentRepository appointmentRepository;
    private final AppointmentMapper appointmentMapper;
    private final UserRepository userRepository;
    private final DoctorRepository doctorRepository;

    @Transactional
    @Override
    public PatientAppointmentResponse changeDoctor(UserDetails userDetails, UUID appointmentId, UUID doctorId) {
        log.info("Change doctor in appointment, patient-email - {}", userDetails.getUsername());
        AppointmentEntity appointment = getPatientAppointment(userDetails, appointmentId);
        DoctorEntity doctor = doctorRepository.findById(doctorId)
                .orElseThrow(DoctorNotFoundException::new);
        appointment.setDoctor(doctor);
        return appointmentMapper.toPatientAppointmentResponse(appointment);
    }

    @Transactional
    @Override
    public PatientAppointmentResponse changeTime(UserDetails userDetails, UUID appointmentId, Instant time) {
        log.info("Change time in appointment, patient-email - {}", userDetails.getUsername());
        AppointmentEntity appointment = getPatientAppointment(userDetails, appointmentId);
        appointment.setStartTime(time);
        return appointmentMapper.toPatientAppointmentResponse(appointment);
    }

    private AppointmentEntity getPatientAppointment(UserDetails userDetails, UUID id){
        UserEntity user = getUserEntityFromUserDetails(userDetails);
        AppointmentEntity appointment = appointmentRepository.findById(id)
                .orElseThrow(AppointmentNotFoundException::new);

        if (!appointment.getPatient().equals(user)) {
            log.info("Checking whether the appointment to the patient, patient-email - {}", userDetails.getUsername());
            throw new AppointmentNotFoundException();
        }
        return appointment;
    }

    private UserEntity getUserEntityFromUserDetails(UserDetails userDetails){
        return userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
    }
}

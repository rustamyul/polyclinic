package ru.itis.service;

import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;

import java.time.Instant;
import java.util.UUID;

public interface PatientService {

    PatientAppointmentResponse changeDoctor(UserDetails userDetails, UUID appointmentId, UUID doctorId);

    PatientAppointmentResponse changeTime(UserDetails userDetails, UUID appointmentId, Instant time);
}

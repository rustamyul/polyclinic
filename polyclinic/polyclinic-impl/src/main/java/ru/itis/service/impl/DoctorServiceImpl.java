package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreateDoctorRequest;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.exception.DoctorNotFoundException;
import ru.itis.exception.FileWrongFormatException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.DoctorEntity;
import ru.itis.model.FileEntity;
import ru.itis.model.UserEntity;
import ru.itis.repository.DoctorRepository;
import ru.itis.repository.UserRepository;
import ru.itis.service.DoctorService;
import ru.itis.service.FileService;
import ru.itis.utils.mapstruct.DoctorMapper;
import ru.itis.utils.mapstruct.FileMapper;

import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class DoctorServiceImpl implements DoctorService {

    private final DoctorRepository doctorRepository;
    private final UserRepository userRepository;

    private final FileService fileService;

    private final FileMapper fileMapper;
    private final DoctorMapper doctorMapper;


    @Override
    public DoctorResponse getDoctorById(UUID id) {
        log.info("Get doctor by id - {}", id);
        return doctorMapper.toDoctorResponse(findDoctorById(id));
    }

    @Override
    public PageOfListResponse<DoctorResponse> getDoctors(int page, int size, Specification<DoctorEntity> spec) {
        log.info("Get list of doctors");
        Page<DoctorEntity> result = doctorRepository.findAll(spec, PageRequest.of(page, size));

        return PageOfListResponse.<DoctorResponse>builder()
                .list(doctorMapper.toDoctorResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    @Override
    public DoctorResponse createDoctor(UserDetails userDetails, CreateDoctorRequest request) {
        log.info("Create doctor account");

        UserEntity user = userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);

        DoctorEntity doctor = doctorRepository.save(
                DoctorEntity.builder()
                        .account(user)
                        .fullName(request.getFullName())
                        .experience(request.getExperience())
                        .education(request.getEducation())
                        .build());

        return doctorMapper.toDoctorResponse(doctor);
    }

    @Transactional
    @Override
    public FileInfoResponse setDoctorPhoto(UserDetails userDetails, UUID photoId) {
        log.info("Set doctor avatar, photo_id - {}, user_email - {}", photoId, userDetails.getUsername());

        DoctorEntity doctor = userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new)
                .getDoctor();

        FileEntity photo = fileService.getFileEntity(photoId);
        if (!photo.getContentType().equals("image/jpeg")) {
            throw new FileWrongFormatException();
        }

        doctor.setPhoto(photo);
        return fileMapper.toFileResponse(photo);
    }

    private DoctorEntity findDoctorById(UUID id){
        return doctorRepository.findById(id)
                .orElseThrow(DoctorNotFoundException::new);
    }

}

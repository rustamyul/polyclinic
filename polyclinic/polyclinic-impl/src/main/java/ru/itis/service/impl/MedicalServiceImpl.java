package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.exception.MedicalServiceNotFoundException;
import ru.itis.model.MedicalServiceEntity;
import ru.itis.repository.MedicalServiceRepository;
import ru.itis.service.MedicalService;
import ru.itis.utils.mapstruct.MedicalServiceMapper;

import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class MedicalServiceImpl implements MedicalService {

    private final MedicalServiceRepository medicalServiceRepository;
    private final MedicalServiceMapper medicalServiceMapper;

    @Override
    public MedicalServiceResponse getMedicalService(UUID id){
        log.info("Get medical service by id - {}", id);
        return medicalServiceMapper.toMedicalServiceResponse(findMedicalServiceById(id));
    }

    @Override
    public PageOfListResponse<MedicalServiceResponse> getMedicalServiceList(int page, int size, Specification<MedicalServiceEntity> spec) {

        log.info("Get list of medical service");
        Page<MedicalServiceEntity> result = medicalServiceRepository.findAll(spec, PageRequest.of(page, size));
        return PageOfListResponse.<MedicalServiceResponse>builder()
                .list(medicalServiceMapper.toMedicalServiceResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    private MedicalServiceEntity findMedicalServiceById(UUID id) {
        return medicalServiceRepository.findById(id)
                .orElseThrow(MedicalServiceNotFoundException::new);
    }

}

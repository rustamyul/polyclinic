package ru.itis.service;

import java.util.Map;

public interface MailSenderService {
    void sendMail(String to, String subject, String templateName, Map<String, String> data);
}

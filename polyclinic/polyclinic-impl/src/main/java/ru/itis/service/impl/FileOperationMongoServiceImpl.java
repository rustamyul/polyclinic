package ru.itis.service.impl;

import com.mongodb.client.gridfs.model.GridFSFile;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsOperations;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.exception.FileNotFoundException;
import ru.itis.exception.FileNotLoadException;
import ru.itis.service.FileOperationService;

import java.io.IOException;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class FileOperationMongoServiceImpl implements FileOperationService {

    private final GridFsOperations gridFsOperations;

    @Override
    public byte[] getFile(String fileId) {
        log.info("Get file from Mongo GridFs through file-id, file-id - {}", fileId);
        GridFSFile file = gridFsOperations.findOne(new Query(Criteria.where("_id").is(fileId)));
        if (file == null) {
            throw new FileNotFoundException();
        }
        try {
            return gridFsOperations.getResource(file)
                    .getInputStream().readAllBytes();
        } catch (IOException e) {
            throw new FileNotLoadException(e);
        }
    }

    @Override
    public String saveFile(MultipartFile multipartFile) {
        log.info("Save new file to Mongo GridFs");
        try {
            return gridFsOperations.store(multipartFile.getInputStream(), multipartFile.getOriginalFilename()).toString();
        } catch (IOException e) {
            throw new FileNotLoadException(e);
        }
    }

    @Override
    public void deleteFiles(List<String> listFileId) {
        log.info("Delete files from Mongo GridFs");
        for (String fileId: listFileId) {
            gridFsOperations.delete(new Query(Criteria.where("_id").is(fileId)));
        }
    }
}

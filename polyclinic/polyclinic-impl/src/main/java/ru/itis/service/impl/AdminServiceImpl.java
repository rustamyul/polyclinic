package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.enums.AccountState;
import ru.itis.enums.CategoryState;
import ru.itis.enums.DoctorState;
import ru.itis.enums.MedicalServiceState;
import ru.itis.exception.CategoryNotFoundException;
import ru.itis.exception.DoctorNotFoundException;
import ru.itis.exception.MedicalServiceNotFoundException;
import ru.itis.model.CategoryEntity;
import ru.itis.model.DoctorEntity;
import ru.itis.model.MedicalServiceEntity;
import ru.itis.repository.CategoryRepository;
import ru.itis.repository.DoctorRepository;
import ru.itis.repository.MedicalServiceRepository;
import ru.itis.service.AdminService;
import ru.itis.utils.mapstruct.CategoryMapper;
import ru.itis.utils.mapstruct.DoctorMapper;
import ru.itis.utils.mapstruct.MedicalServiceMapper;

import javax.transaction.Transactional;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class AdminServiceImpl implements AdminService {

    private final DoctorRepository doctorRepository;
    private final DoctorMapper doctorMapper;

    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    private final MedicalServiceRepository medicalServiceRepository;
    private final MedicalServiceMapper medicalServiceMapper;

    @Transactional
    @Override
    public DoctorResponse editDoctorInformation(UUID id, EditDoctorInfoRequest request) {
        log.info("Update doctor information, doctor-id - {}", id);;

        DoctorEntity doctor = findDoctorById(id);
        doctorMapper.updateDoctor(request, doctor);

        return doctorMapper.toDoctorResponse(doctor);
    }

    @Transactional
    @Override
    public DeleteResponse deleteDoctor(UUID id) {
        log.info("Delete doctor account by id, id - {}", id);
        DoctorEntity doctor = findDoctorById(id);
        doctor.getAccount().setState(AccountState.DELETED);
        doctor.setState(DoctorState.DELETED);
        return new DeleteResponse();
    }

    @Transactional
    @Override
    public CategoryResponse editCategory(UUID id, EditCategoryRequest request) {
        log.info("Edit category, category_id - {}", id);

        CategoryEntity category = findCategoryById(id);
        categoryMapper.updateCategory(request, category);
        return categoryMapper.toCategoryResponse(category);
    }

    @Transactional
    @Override
    public DeleteResponse deleteCategory(UUID id) {
        log.info("Delete category, category_id - {}", id);

        CategoryEntity category = findCategoryById(id);
        category.setState(CategoryState.DELETED);
        return new DeleteResponse();
    }

    @Transactional
    @Override
    public MedicalServiceResponse editMedicalService(UUID id, EditMedicalServiceRequest request){
        log.info("Edit medical service through id, id - {}", id);
        MedicalServiceEntity medicalService = findMedicalServiceById(id);
        medicalServiceMapper.updateMedicalServiceEntity(request, medicalService);
        return medicalServiceMapper.toMedicalServiceResponse(medicalService);
    }

    @Transactional
    @Override
    public DeleteResponse deleteMedicalService(UUID id) {
        log.info("Delete medical service by id, id - {}", id);
        MedicalServiceEntity medicalService = findMedicalServiceById(id);
        medicalService.setState(MedicalServiceState.DELETED);
        return new DeleteResponse();
    }

    private MedicalServiceEntity findMedicalServiceById(UUID id) {
        return medicalServiceRepository.findById(id)
                .orElseThrow(MedicalServiceNotFoundException::new);
    }

    private CategoryEntity findCategoryById(UUID id) {
        return categoryRepository.findById(id)
                .orElseThrow(CategoryNotFoundException::new);
    }

    private DoctorEntity findDoctorById(UUID id){
        return doctorRepository.findById(id)
                .orElseThrow(DoctorNotFoundException::new);
    }
}

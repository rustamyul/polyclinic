package ru.itis.service;

import org.springframework.web.multipart.MultipartFile;
import ru.itis.dto.response.ContentResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.model.FileEntity;

import java.util.UUID;

public interface FileService {

    FileInfoResponse getFileInfo(UUID uuid);

    ContentResponse getFile(UUID uuid);

    FileInfoResponse saveFile(MultipartFile multipartFile);

    FileEntity getFileEntity(UUID uuid);

    void deleteFile(FileEntity file);
}

package ru.itis.service;

import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.PageOfListResponse;

import java.util.UUID;

public interface CategoryService {

    PageOfListResponse<CategoryResponse> getCategoryList(int page, int size);

    CategoryResponse getCategoryById(UUID id);;
}

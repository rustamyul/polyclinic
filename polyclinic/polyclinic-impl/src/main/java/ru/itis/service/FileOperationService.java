package ru.itis.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;


public interface FileOperationService {

    byte[] getFile(String fileId);

    String saveFile(MultipartFile multipartFile);

    void deleteFiles(List<String> collect);
}

package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import ru.itis.dto.response.appointment.FreeAppointmentResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.FreeAppointmentEntity;

public interface FreeAppointmentService {

    PageOfListResponse<FreeAppointmentResponse> getFreeAppointments(int page, int size, Specification<FreeAppointmentEntity> spec);
}

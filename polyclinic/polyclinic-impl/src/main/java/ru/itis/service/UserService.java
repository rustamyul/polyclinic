package ru.itis.service;

import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.SignInForm;
import ru.itis.dto.response.CreateUserResponse;
import ru.itis.model.UserEntity;

public interface UserService {

    UserEntity getUserBySubject(String email);

    UserEntity login(SignInForm signInForm);

    CreateUserResponse createUser(CreateUserRequest userRequest);
}

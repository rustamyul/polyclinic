package ru.itis.service;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import ru.itis.dto.request.CreateDoctorRequest;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.DoctorEntity;

import java.util.UUID;

public interface DoctorService {

    DoctorResponse getDoctorById(UUID id);

    DoctorResponse createDoctor(UserDetails userDetails, CreateDoctorRequest request);

    FileInfoResponse setDoctorPhoto(UserDetails userDetails, UUID photoId);

    PageOfListResponse<DoctorResponse> getDoctors(int page, int size, Specification<DoctorEntity> spec);

}


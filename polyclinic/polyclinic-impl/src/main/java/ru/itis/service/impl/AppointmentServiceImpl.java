package ru.itis.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ru.itis.dto.request.CreateAppointmentRequest;
import ru.itis.dto.request.EditAppointmentRequest;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.enums.AppointmentState;
import ru.itis.enums.FreeAppointmentState;
import ru.itis.exception.AppointmentNotFoundException;
import ru.itis.exception.FreeAppointmentNotFoundException;
import ru.itis.exception.MedicalServiceNotFoundException;
import ru.itis.exception.user.UserNotFoundException;
import ru.itis.model.*;
import ru.itis.repository.*;
import ru.itis.service.*;
import ru.itis.utils.mapstruct.AppointmentMapper;
import ru.itis.utils.mapstruct.FileMapper;

import javax.transaction.Transactional;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


@Slf4j
@Service
@RequiredArgsConstructor
public class AppointmentServiceImpl implements AppointmentService {

    public static final String APPOINTMENT = "appointment";

    private final AppointmentRepository appointmentRepository;
    private final UserRepository userRepository;
    private final DoctorRepository doctorRepository;
    private final MedicalServiceRepository medicalServiceRepository;
    private final FreeAppointmentRepository freeAppointmentRepository;

    private final MailSenderService mailSenderService;
    private final SmsSenderService smsSenderService;
    private final FileService fileService;

    private final AppointmentMapper appointmentMapper;
    private final FileMapper fileMapper;

    @Value("${name.mail.template}")
    private String emailTemplaName;

    @Value("${spring.profiles.active}")
    private String activeProfile;

    @Override
    public PatientAppointmentResponse createAppointment(UserDetails userDetails, CreateAppointmentRequest requestForm) {
        log.info("Create appointment, patient_email - {}", userDetails.getUsername());

        UserEntity patient = getUserEntityFromUserDetails(userDetails);
        DoctorEntity doctor = doctorRepository.findById(requestForm.getDoctorId())
                .orElseThrow(UserNotFoundException::new);
        MedicalServiceEntity medicalServiceEntity = medicalServiceRepository.findById(requestForm.getMedicalServiceId())
                .orElseThrow(MedicalServiceNotFoundException::new);

        changeFreeAppointmentState(doctor.getId(),
                requestForm.getStartAppointment(),
                FreeAppointmentState.BUSY);

        AppointmentEntity appointment = AppointmentEntity.builder()
                .doctor(doctor)
                .patient(patient)
                .medicalService(medicalServiceEntity)
                .comment(requestForm.getComment())
                .startTime(requestForm.getStartAppointment())
                .state(AppointmentState.ACTIVE)
                .build();

        if (activeProfile.equals("prod")){
            toSendSms(appointment);
        } else if (activeProfile.equals("dev")){
            toSendLetterEmail(appointment);
        }

        return appointmentMapper.toPatientAppointmentResponse(appointmentRepository.save(appointment));
    }

    @Transactional
    @Override
    public DeleteResponse cancelAppointment(UserDetails userDetails, UUID id) {
        log.info("Cancel appointment, patient_email - {}", userDetails.getUsername());

        UserEntity patient = getUserEntityFromUserDetails(userDetails);

        AppointmentEntity appointment = findAppointmentById(id);

        if (!appointment.getPatient().equals(patient)) {
            throw new AppointmentNotFoundException();
        }

        changeFreeAppointmentState(appointment.getDoctor().getId(),
                appointment.getStartTime(),
                FreeAppointmentState.FREE);

        appointment.setState(AppointmentState.CANCEL);
        return new DeleteResponse();
    }

    @Transactional
    @Override
    public FileInfoResponse setFileForAppointment(UserDetails userDetails, UUID id, UUID fileId) {
        log.info("Set file for appointment, patient_email - {}", userDetails.getUsername());

        UserEntity patient = getUserEntityFromUserDetails(userDetails);

        AppointmentEntity appointment = findAppointmentById(id);

        if (!appointment.getPatient().equals(patient)) {
            throw new AppointmentNotFoundException();
        }
        FileEntity file = fileService.getFileEntity(fileId);

        appointment.getFiles().add(file);
        return fileMapper.toFileResponse(file);
    }

    @Transactional
    @Override
    public DoctorAppointmentResponse editAppointmentInfo(UserDetails userDetails, UUID id, EditAppointmentRequest request) {
        log.info("Edit appointment info, doctor_email - {}", userDetails.getUsername());

        DoctorEntity doctor = getUserEntityFromUserDetails(userDetails).getDoctor();

        AppointmentEntity appointment = findAppointmentById(id);

        if (!appointment.getDoctor().equals(doctor)) {
            throw new AppointmentNotFoundException();
        }
        FileEntity file = fileService.getFileEntity(request.getFileId());
        appointment.getFiles().add(file);
        appointmentMapper.updateMedicalServiceEntity(request, appointment);

        return appointmentMapper.toDoctorAppointmentResponse(appointment);
    }

    @Override
    public PageOfListResponse<DoctorAppointmentResponse> getDoctorAppointments(UserDetails userDetails, int page, int size, Specification<AppointmentEntity> spec) {
        log.info("Get list of doctor appointments, doctor-email - {}", userDetails.getUsername());
        UserEntity user = getUserEntityFromUserDetails(userDetails);

        Specification<AppointmentEntity> spec1 = (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("doctor"),user.getDoctor());

        Page<AppointmentEntity> result = appointmentRepository.findAll(spec1.and(spec), PageRequest.of(page, size));
        return PageOfListResponse.<DoctorAppointmentResponse>builder()
                .list(appointmentMapper.toDoctorAppointmentResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    @Override
    public PageOfListResponse<PatientAppointmentResponse> getPatientAppointments(UserDetails userDetails, int page, int size, Specification<AppointmentEntity> spec) {
        log.info("Get list of patient appointments, patient-email - {}", userDetails.getUsername());
        UserEntity user = getUserEntityFromUserDetails(userDetails);

        Specification<AppointmentEntity> spec1 = (root, query, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("patient"),user);

        Page<AppointmentEntity> result = appointmentRepository.findAll(spec1.and(spec), PageRequest.of(page, size));

        return PageOfListResponse.<PatientAppointmentResponse>builder()
                .list(appointmentMapper.toPatientAppointmentResponseList(result.getContent()))
                .totalPage(result.getTotalPages())
                .size(size)
                .build();
    }

    void changeFreeAppointmentState(UUID doctorId, Instant startTime, FreeAppointmentState state){
        FreeAppointmentEntity freeAppointment = freeAppointmentRepository.findByDoctor_IdAndStartTime(doctorId, startTime)
                .orElseThrow(FreeAppointmentNotFoundException::new);
        freeAppointment.setState(state);
    }


    private AppointmentEntity findAppointmentById(UUID id){
        return appointmentRepository.findById(id)
                .orElseThrow(AppointmentNotFoundException::new);
    }

    private UserEntity getUserEntityFromUserDetails(UserDetails userDetails){
        return userRepository.findByEmail(userDetails.getUsername())
                .orElseThrow(UserNotFoundException::new);
    }

    private void toSendSms(AppointmentEntity appointment) {
        Runnable r = () -> {
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            String phone = appointment.getPatient().getPhone();
            String doctor = appointment.getDoctor().getFullName();
            String date = LocalDate.ofInstant(appointment.getStartTime(), ZoneId.of("Europe/Moscow")).toString();
            String time =  timeFormat.format(Date.from(appointment.getStartTime()));
            String message = "Ваш прием состоится "+ date + "в" + time +
                    "\nДоктор - " + doctor;
            smsSenderService.sendSms(phone, message);
        };
        Thread thread = new Thread(r);
        thread.start();
    }

    private void toSendLetterEmail(AppointmentEntity appointment) {
        Runnable r = () -> {
            String email = appointment.getPatient().getEmail();
            Map<String, String> data = new HashMap<>();
            SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
            data.put("doctor", appointment.getDoctor().getFullName());
            data.put("date", LocalDate.ofInstant(appointment.getStartTime(), ZoneId.of("Europe/Moscow")).toString());
            data.put("time", timeFormat.format(Date.from(appointment.getStartTime())));
            mailSenderService.sendMail(email, APPOINTMENT, emailTemplaName, data);
        };
        Thread thread = new Thread(r);
        thread.start();
    }
}

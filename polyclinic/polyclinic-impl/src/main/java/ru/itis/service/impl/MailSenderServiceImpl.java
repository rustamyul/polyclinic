package ru.itis.service.impl;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import ru.itis.exception.SendMailFailException;
import ru.itis.service.MailSenderService;

import java.io.IOException;
import java.util.Map;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailSenderServiceImpl implements MailSenderService {

    private final JavaMailSender mailSender;
    private final Configuration configuration;

    @Value("${spring.mail.username}")
    private String from;


    @Override
    public void sendMail(String to, String subject, String templateName, Map<String, String> data) {

        log.info("Send mail-message to confirm the user, user-mail - {}", to);
        MimeMessagePreparator preparatory;
        try {
            final String mailText = FreeMarkerTemplateUtils
                    .processTemplateIntoString(configuration.getTemplate(templateName, "UTF-8"), data);
            preparatory = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setSubject(subject);
                messageHelper.setText(mailText, true);
                messageHelper.setTo(to);
                messageHelper.setFrom(from);
            };
            mailSender.send(preparatory);
        } catch (TemplateException | IOException e) {
            throw new SendMailFailException();
        }
    }
}

package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.itis.enums.FreeAppointmentState;
import ru.itis.model.FreeAppointmentEntity;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;

public interface FreeAppointmentRepository extends JpaRepository<FreeAppointmentEntity, UUID>, JpaSpecificationExecutor<FreeAppointmentEntity> {

    Optional<FreeAppointmentEntity> findByDoctor_IdAndStartTime(UUID doctorId, Instant time);

    void deleteAllByStartTimeBefore(Instant time);
}

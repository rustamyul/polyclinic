package ru.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.itis.model.MedicalServiceEntity;

import java.util.UUID;

public interface MedicalServiceRepository extends JpaRepository<MedicalServiceEntity, UUID>, JpaSpecificationExecutor<MedicalServiceEntity> {

}

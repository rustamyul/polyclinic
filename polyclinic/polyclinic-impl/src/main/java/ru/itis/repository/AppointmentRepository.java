package ru.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.model.AppointmentEntity;
import ru.itis.model.UserEntity;

import java.util.UUID;

public interface AppointmentRepository extends JpaRepository<AppointmentEntity, UUID>, JpaSpecificationExecutor<AppointmentEntity> {
    Page<AppointmentEntity> findByPatient(UserEntity user, PageRequest of);
}

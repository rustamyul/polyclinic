package ru.itis.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ru.itis.model.DoctorEntity;
import ru.itis.model.MedicalServiceEntity;
import ru.itis.model.UserEntity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface DoctorRepository extends JpaRepository<DoctorEntity, UUID>, JpaSpecificationExecutor<DoctorEntity> {

    Optional<DoctorEntity> getDoctorEntityByAccount(UserEntity user);

    Page<DoctorEntity> findAllByMedicalServicesIsContaining(MedicalServiceEntity entity, Pageable pageable);

    Page<DoctorEntity> findAllByMedicalServicesIsIn(List<MedicalServiceEntity> entities, Pageable pageable);
}

package ru.itis.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.itis.enums.FileState;
import ru.itis.model.FileEntity;

import java.util.List;
import java.util.UUID;

public interface FileRepository extends JpaRepository<FileEntity, UUID> {
    List<FileEntity> findAllByState(FileState deleted);
}



package ru.itis.utils.mapstruct;

import org.mapstruct.*;
import ru.itis.dto.request.EditAppointmentRequest;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.model.AppointmentEntity;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class, DoctorMapper.class})
public interface AppointmentMapper {

    @Mapping(target = "files", source = "files", qualifiedByName = "Info")
    DoctorAppointmentResponse toDoctorAppointmentResponse(AppointmentEntity appointment);

    @Mapping(target = "files", source = "files", qualifiedByName = "Info")
    PatientAppointmentResponse toPatientAppointmentResponse(AppointmentEntity appointment);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateMedicalServiceEntity(EditAppointmentRequest request, @MappingTarget AppointmentEntity entity);

    List<PatientAppointmentResponse> toPatientAppointmentResponseList(List<AppointmentEntity> entities);

    List<DoctorAppointmentResponse> toDoctorAppointmentResponseList(List<AppointmentEntity> entities);

}

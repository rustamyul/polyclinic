package ru.itis.utils.mapstruct;

import org.mapstruct.*;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.model.MedicalServiceEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface MedicalServiceMapper {
    List<MedicalServiceResponse> toMedicalServiceResponseList(List<MedicalServiceEntity> medicalServiceEntities);

    MedicalServiceResponse toMedicalServiceResponse(MedicalServiceEntity medicalServiceEntity);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateMedicalServiceEntity(EditMedicalServiceRequest request, @MappingTarget MedicalServiceEntity entity);
}

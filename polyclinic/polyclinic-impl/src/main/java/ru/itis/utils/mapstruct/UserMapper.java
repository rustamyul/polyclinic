package ru.itis.utils.mapstruct;

import org.mapstruct.Mapper;
import ru.itis.dto.response.CreateUserResponse;
import ru.itis.model.UserEntity;

@Mapper(componentModel = "spring")
public interface UserMapper {

    CreateUserResponse toCreateUserRequest(UserEntity user);
}

package ru.itis.utils.mapstruct;

import org.mapstruct.*;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.model.DoctorEntity;

import java.util.List;

@Mapper(componentModel = "spring", uses = {FileMapper.class})
public interface DoctorMapper {

    @Mapping(target = "photo", source = "photo", qualifiedByName = "Info")
    DoctorResponse toDoctorResponse(DoctorEntity doctorEntity);

    List<DoctorResponse> toDoctorResponseList(List<DoctorEntity> doctorEntities);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateDoctor(EditDoctorInfoRequest request, @MappingTarget DoctorEntity doctorEntity);

}

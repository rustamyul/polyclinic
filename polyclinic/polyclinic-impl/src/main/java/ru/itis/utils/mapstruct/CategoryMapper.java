package ru.itis.utils.mapstruct;

import org.mapstruct.*;
import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.model.CategoryEntity;

import java.util.List;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryResponse toCategoryResponse(CategoryEntity entity);

    List<CategoryResponse> toCategoryResponseList(List<CategoryEntity> content);

    @BeanMapping(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE,
            nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
    void updateCategory(EditCategoryRequest request, @MappingTarget CategoryEntity category);
}

package ru.itis.utils.mapstruct;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import ru.itis.dto.response.ContentResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.model.FileEntity;

@Mapper(componentModel = "spring")
public interface FileMapper {

    @Named("Info")
    @Mapping(target = "fileId", source = "id")
    @Mapping(target = "fileSize", source = "size")
    FileInfoResponse toFileResponse(FileEntity file);

    @Named("Content")
    @Mapping(target = "fileId", source = "id")
    @Mapping(target = "fileSize", source = "size")
    ContentResponse toContentResponse(FileEntity fileEntity);
}

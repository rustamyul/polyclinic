package ru.itis.utils.mapstruct;

import org.mapstruct.Mapper;
import ru.itis.dto.response.appointment.FreeAppointmentResponse;
import ru.itis.model.FreeAppointmentEntity;

import java.util.List;


@Mapper(componentModel = "spring", uses = {DoctorMapper.class})
public interface FreeAppointmentMapper {

    FreeAppointmentResponse toFreeAppointmentResponse(FreeAppointmentEntity entity);

    List<FreeAppointmentResponse> toFreeAppointmentResponseList(List<FreeAppointmentEntity> entities);
}

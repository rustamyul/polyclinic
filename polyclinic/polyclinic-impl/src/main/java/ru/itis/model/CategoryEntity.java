package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import org.hibernate.annotations.Where;
import ru.itis.enums.CategoryState;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "category")
@Where(clause = "state='CREATED'")
public class CategoryEntity extends AbstractEntity{

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private CategoryState state;

    @OneToMany(mappedBy = "category")
    private List<MedicalServiceEntity> medicalServices;
}

package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.FileState;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "file")
public class FileEntity extends AbstractEntity{


    @Column(name = "file_id", nullable = false, unique = true)
    private String fileId;

    @Column(name = "file_name", nullable = false)
    private String filename;

    @Column(name = "size", nullable = false)
    private Long size;

    @Column(name = "content_type", nullable = false)
    private String contentType;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private FileState state;

    @OneToMany(mappedBy = "photo", fetch = LAZY)
    private List<DoctorEntity> doctorEntities;

    @ManyToOne
    @JoinColumn(name = "appointment_id", referencedColumnName = "id")
    private AppointmentEntity appointment;
}

package ru.itis.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.FreeAppointmentState;

import javax.persistence.*;
import java.time.Instant;

@EqualsAndHashCode(callSuper = true)
@Data
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "free_appointment")
public class FreeAppointmentEntity extends AbstractEntity{

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private FreeAppointmentState state;

    @ManyToOne
    @JoinColumn(name = "doctor_id", referencedColumnName = "id")
    private DoctorEntity doctor;
}

package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.CategoryState;
import ru.itis.enums.DoctorState;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "doctor")
public class DoctorEntity extends AbstractEntity{

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "experience")
    private String experience;

    @Column(name = "education")
    private String education;

    @ManyToOne
    @JoinColumn(name = "photo_id")
    private FileEntity photo;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private DoctorState state;

    @OneToOne
    @JoinColumn(name = "account_id")
    private UserEntity account;

    @OneToMany(mappedBy = "doctor", fetch = LAZY)
    private List<AppointmentEntity> appointments;

    @OneToMany(mappedBy = "doctor", fetch = LAZY)
    private List<FreeAppointmentEntity> freeAppointments;

    @ManyToMany
    @JoinTable(name = "doctor_service",
            joinColumns = {@JoinColumn(name = "doctor_id", referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "medical_service_id", referencedColumnName = "id")})
    private List<MedicalServiceEntity> medicalServices;
}

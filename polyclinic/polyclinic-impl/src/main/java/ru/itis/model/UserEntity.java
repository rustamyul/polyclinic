package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.AccountRole;
import ru.itis.enums.AccountState;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REMOVE;
import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "account")
public class UserEntity extends AbstractEntity{

    @Column(name = "email", unique = true, nullable = false)
    private String email;

    @Column(name = "hash_password")
    private String hashPassword;

    @Column(name = "phone", unique = true)
    private String phone;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private AccountState state;

    @Column(name = "role")
    @Enumerated(value = EnumType.STRING)
    private AccountRole role;

    @OneToOne(mappedBy = "account", cascade = {PERSIST, REMOVE},
            fetch = FetchType.LAZY, optional = false)
    private DoctorEntity doctor;

    @OneToMany(mappedBy = "patient", fetch = LAZY)
    private List<AppointmentEntity> appointmentEntities;
}
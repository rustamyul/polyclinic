package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.MedicalServiceState;

import javax.persistence.*;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "medical_service")
public class MedicalServiceEntity extends AbstractEntity{

    @Column(name = "title")
    private String title;

    @Column(name = "cost")
    private Integer cost;

    @Column(name = "description")
    private String description;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private MedicalServiceState state;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private CategoryEntity category;

    @OneToMany(mappedBy = "medicalService", fetch = LAZY)
    private List<AppointmentEntity> appointmentEntities;

    @ManyToMany(mappedBy = "medicalServices")
    private List<DoctorEntity> doctorEntities;
}

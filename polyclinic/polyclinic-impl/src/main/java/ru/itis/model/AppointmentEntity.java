package ru.itis.model;

import lombok.*;
import lombok.experimental.SuperBuilder;
import ru.itis.enums.AppointmentState;

import javax.persistence.*;
import java.time.Instant;
import java.util.List;

import static javax.persistence.FetchType.LAZY;

@EqualsAndHashCode(callSuper = true)
@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "appointment")
public class AppointmentEntity extends AbstractEntity{

    @Column(name = "start_time")
    private Instant startTime;

    @Column(name = "comment")
    private String comment;

    @Column(name = "recommendation")
    private String recommendation;

    @Column(name = "description")
    private String description;

    @Column(name = "medical_history")
    private String medicalHistory;

    @Column(name = "file_links")
    private String fileLinks;

    @Column(name = "state")
    @Enumerated(value = EnumType.STRING)
    private AppointmentState state;

    @ManyToOne
    @JoinColumn(name = "doctor_id", referencedColumnName = "id")
    private DoctorEntity doctor;

    @ManyToOne
    @JoinColumn(name = "patient_id", referencedColumnName = "id")
    private UserEntity patient;

    @ManyToOne
    @JoinColumn(name = "medical_service_id", referencedColumnName = "id")
    private MedicalServiceEntity medicalService;

    @OneToMany(mappedBy = "appointment", fetch = LAZY)
    private List<FileEntity> files;

}

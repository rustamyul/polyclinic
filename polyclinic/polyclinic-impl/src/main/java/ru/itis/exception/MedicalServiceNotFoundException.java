package ru.itis.exception;

public class MedicalServiceNotFoundException extends ApplicationNotFoundException{

    public MedicalServiceNotFoundException() {
        super("Medical service not found");
    }
}

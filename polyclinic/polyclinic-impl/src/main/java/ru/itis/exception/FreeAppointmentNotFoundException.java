package ru.itis.exception;

public class FreeAppointmentNotFoundException extends ApplicationNotFoundException{

    public FreeAppointmentNotFoundException() {
        super("Free appointment not found");
    }
}

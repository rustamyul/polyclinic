package ru.itis.exception;

public class AuthenticationHeaderException extends RuntimeException {

    public AuthenticationHeaderException(String message) {
        super(message);
    }
}

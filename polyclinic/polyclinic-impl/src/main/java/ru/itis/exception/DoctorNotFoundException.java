package ru.itis.exception;

public class DoctorNotFoundException extends ApplicationNotFoundException {

    public DoctorNotFoundException() {
        super("Doctor not found");
    }
}

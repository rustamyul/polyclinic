package ru.itis.exception;


import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
public class ApplicationServiceException extends RuntimeException{

    private final HttpStatus httpStatus;

    public ApplicationServiceException(HttpStatus httpStatus, String message) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
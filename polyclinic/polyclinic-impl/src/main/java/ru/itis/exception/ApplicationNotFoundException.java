package ru.itis.exception;

import org.springframework.http.HttpStatus;

public class ApplicationNotFoundException extends ApplicationServiceException{

    public ApplicationNotFoundException(String message) {
        super(HttpStatus.NOT_FOUND, message);
    }
}

package ru.itis.exception.user;

import lombok.Getter;
import ru.itis.exception.ApplicationNotFoundException;

@Getter
public class UserNotFoundException extends ApplicationNotFoundException {

    public UserNotFoundException() {
        super("User not found");
    }
}

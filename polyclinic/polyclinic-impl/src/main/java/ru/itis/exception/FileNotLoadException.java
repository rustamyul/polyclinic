package ru.itis.exception;

public class FileNotLoadException extends RuntimeException {

    public FileNotLoadException(Exception exception) {
        super("File didn't loading" + exception);
    }
}

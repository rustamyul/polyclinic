package ru.itis.exception;

import org.springframework.http.HttpStatus;

public class SendMailFailException extends ApplicationServiceException {

    public SendMailFailException() {
        super(HttpStatus.INTERNAL_SERVER_ERROR, "Sending a message by email is a failure");
    }
}

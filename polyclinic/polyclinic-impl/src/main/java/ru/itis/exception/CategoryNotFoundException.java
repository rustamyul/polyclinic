package ru.itis.exception;

public class CategoryNotFoundException extends ApplicationNotFoundException{

    public CategoryNotFoundException() {
        super("Category not found");
    }
}

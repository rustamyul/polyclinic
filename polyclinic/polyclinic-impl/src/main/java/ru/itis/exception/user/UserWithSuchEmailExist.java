package ru.itis.exception.user;

import org.springframework.http.HttpStatus;
import ru.itis.exception.ApplicationServiceException;

public class UserWithSuchEmailExist extends ApplicationServiceException {

    public UserWithSuchEmailExist() {
        super(HttpStatus.BAD_REQUEST, "User with such an email already exists");
    }
}

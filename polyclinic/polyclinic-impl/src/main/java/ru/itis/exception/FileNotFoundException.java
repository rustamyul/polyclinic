package ru.itis.exception;

public class FileNotFoundException extends ApplicationNotFoundException {

    public FileNotFoundException() {
        super("File not found");
    }
}

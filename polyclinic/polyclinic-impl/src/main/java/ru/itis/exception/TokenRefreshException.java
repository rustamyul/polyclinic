package ru.itis.exception;

import org.springframework.http.HttpStatus;
import ru.itis.exception.ApplicationServiceException;

public class TokenRefreshException extends ApplicationServiceException {

    public TokenRefreshException(String token, String message) {
        super(HttpStatus.UNAUTHORIZED, String.format("Failed for [%s]: %s", token, message));
    }
}

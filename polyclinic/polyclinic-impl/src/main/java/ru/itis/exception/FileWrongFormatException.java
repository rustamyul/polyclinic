package ru.itis.exception;

import org.springframework.http.HttpStatus;

public class FileWrongFormatException extends ApplicationServiceException{
    public FileWrongFormatException() {
        super(HttpStatus.BAD_REQUEST, "File format is incorrect");
    }
}

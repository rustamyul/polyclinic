package ru.itis.exception;

public class AppointmentNotFoundException extends ApplicationNotFoundException{

    public AppointmentNotFoundException() {
        super("Appointment not found");
    }
}

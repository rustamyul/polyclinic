package ru.itis.exception.user;

import org.springframework.http.HttpStatus;
import ru.itis.exception.ApplicationServiceException;

public class PasswordIsIncorrectException extends ApplicationServiceException {
    public PasswordIsIncorrectException(String message) {
        super(HttpStatus.BAD_REQUEST, message);
    }
}

package ru.itis.jwt;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.jwt.provider.JwtAccessTokenProvider;
import ru.itis.jwt.provider.JwtRefreshTokenProvider;
import ru.itis.model.AccountRefreshTokenEntity;
import ru.itis.model.UserEntity;

import java.util.Collections;
import java.util.UUID;

import static ru.itis.consts.PolyclinicConstants.BEARER;
import static ru.itis.consts.PolyclinicConstants.ROLE;

@Service
@RequiredArgsConstructor
public class JwtTokenServiceImpl implements JwtTokenService {

    private final JwtAccessTokenProvider jwtAccessTokenProvider;
    private final JwtRefreshTokenProvider jwtRefreshTokenProvider;

    @Override
    public boolean tokenIsValid(String token) {
        return jwtAccessTokenProvider.validateAccessToken(token);
    }

    @Override
    public UserEntity getUserByToken(String token) {
        return jwtAccessTokenProvider.getUserByToken(token);
    }

    @Override
    public TokenCoupleResponse generateTokenCouple(UserEntity user) {

        String accessToken = jwtAccessTokenProvider
                .generateAccessToken(user.getEmail(), Collections.singletonMap(ROLE, user.getRole().toString()));

        UUID refreshToken = jwtRefreshTokenProvider.generateRefreshToken(user);

        return TokenCoupleResponse.builder()
                .accessToken(BEARER.concat(accessToken))
                .refreshToken(refreshToken)
                .expireDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken).toInstant())
                .build();
    }

    @Override
    public TokenCoupleResponse refreshAccessToken(TokenCoupleResponse tokenCoupleResponse) {
        String role = jwtAccessTokenProvider.getRoleFromAccessToken(tokenCoupleResponse.getAccessToken().substring(BEARER.length()));

        AccountRefreshTokenEntity verifiedRefreshToken = jwtRefreshTokenProvider.verifyRefreshTokenExpiration(tokenCoupleResponse.getRefreshToken());

        String accessToken = jwtAccessTokenProvider.generateAccessToken(
                jwtAccessTokenProvider.getSubjectFromAccessToken(tokenCoupleResponse.getAccessToken().substring(BEARER.length())),
                Collections.singletonMap(ROLE, role));

        return TokenCoupleResponse.builder()
                .refreshToken(verifiedRefreshToken.getId())
                .accessToken(BEARER.concat(accessToken))
                .expireDate(jwtAccessTokenProvider.getExpirationDateFromAccessToken(accessToken).toInstant())
                .build();
    }
}

package ru.itis.jwt;

import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.model.UserEntity;

public interface JwtTokenService {

    boolean tokenIsValid(String token);

    UserEntity getUserByToken(String token);

    TokenCoupleResponse generateTokenCouple(UserEntity user);

    TokenCoupleResponse refreshAccessToken(TokenCoupleResponse tokenCoupleResponse);
}

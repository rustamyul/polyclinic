package ru.itis.jwt.provider.impl;

import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.itis.exception.AuthenticationHeaderException;
import ru.itis.jwt.provider.JwtAccessTokenProvider;
import ru.itis.model.UserEntity;
import ru.itis.service.UserService;

import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static ru.itis.consts.PolyclinicConstants.ROLE;

@Component
@RequiredArgsConstructor
public class JwtAccessTokenProviderImpl implements JwtAccessTokenProvider {


    private final UserService userService;

    @Value("${jwt.secret}")
    private String jwtSecret;

    @Value("${jwt.expiration.access.mills}")
    private long expirationAccessInMills;

    @Override
    public String generateAccessToken(String subject, Map<String, Object> data) {
        Map<String, Object> claims = new HashMap<>(data);
        claims.put(Claims.SUBJECT, subject);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(Date.from(Instant.now().plusMillis(expirationAccessInMills)))
                .signWith(SignatureAlgorithm.HS512, jwtSecret)
                .compact();
    }

    @Override
    public boolean validateAccessToken(String token) {
        try {
            Date date = getBody(token).getExpiration();
            return date.after(new Date());
        } catch (ExpiredJwtException e) {
            throw new AuthenticationHeaderException("Token expired date error");
        }
    }

    @Override
    public UserEntity getUserByToken(String token) {
        return userService.getUserBySubject(getSubjectFromAccessToken(token));
    }

    @Override
    public String getRoleFromAccessToken(String token) {
        try {
            return String.valueOf(getBody(token)
                    .get(ROLE));
        } catch (ExpiredJwtException e) {
            return String.valueOf(e.getClaims()
                    .get(ROLE));
        }
    }

    @Override
    public Date getExpirationDateFromAccessToken(String token) {
        try {
            return getBody(token)
                    .getExpiration();
        } catch (ExpiredJwtException e) {
            return e.getClaims().getExpiration();
        }
    }

    @Override
    public String getSubjectFromAccessToken(String token) {
        try {
            return getBody(token)
                    .getSubject();
        } catch (ExpiredJwtException e) {
            return e.getClaims().getSubject();
        }
    }

    private Claims getBody(String token){
        return Jwts.parser()
                .setSigningKey(jwtSecret)
                .parseClaimsJws(token)
                .getBody();
    }
}

package ru.itis.jwt.provider;

import ru.itis.model.AccountRefreshTokenEntity;
import ru.itis.model.UserEntity;

import java.util.UUID;

public interface JwtRefreshTokenProvider {

    UUID generateRefreshToken(UserEntity user);

    AccountRefreshTokenEntity verifyRefreshTokenExpiration(UUID refreshToken);

}

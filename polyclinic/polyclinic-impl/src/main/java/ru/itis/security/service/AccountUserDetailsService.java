package ru.itis.security.service;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.*;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.stereotype.Service;
import ru.itis.enums.AccountState;
import ru.itis.model.UserEntity;

@Service
@RequiredArgsConstructor
public class AccountUserDetailsService implements AuthenticationUserDetailsService<PreAuthenticatedAuthenticationToken> {

    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserDetails(PreAuthenticatedAuthenticationToken token) throws UsernameNotFoundException {
        UserEntity user = (UserEntity) token.getPrincipal();
        return User.builder()
                .username(user.getEmail())
                .password(passwordEncoder.encode(user.getHashPassword()))
                .authorities(user.getRole().toString())
                .accountExpired(user.getState().equals(AccountState.DELETED))
                .build();
    }
}

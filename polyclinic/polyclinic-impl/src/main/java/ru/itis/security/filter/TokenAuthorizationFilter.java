package ru.itis.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationToken;
import org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter;
import ru.itis.exception.AuthenticationHeaderException;
import ru.itis.jwt.JwtTokenService;
import ru.itis.model.UserEntity;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.itis.consts.PolyclinicConstants.BEARER;

public class TokenAuthorizationFilter extends RequestHeaderAuthenticationFilter {

    private final JwtTokenService jwtTokenService;
    private final ObjectMapper objectMapper;

    public TokenAuthorizationFilter(AuthenticationManager authenticationManager, JwtTokenService jwtTokenService, ObjectMapper objectMapper) {
        this.jwtTokenService = jwtTokenService;
        this.objectMapper = objectMapper;
        this.setAuthenticationManager(authenticationManager);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        try {
            String token = getTokenFromHeader(((HttpServletRequest)request).getHeader(AUTHORIZATION));
            if (token != null) {
                if (jwtTokenService.tokenIsValid(token)) {
                    UserEntity user = jwtTokenService.getUserByToken(token);
                    PreAuthenticatedAuthenticationToken preAuthenticatedAuthenticationToken = new PreAuthenticatedAuthenticationToken(user, token);

                    if (Objects.isNull(SecurityContextHolder.getContext().getAuthentication())) {
                        SecurityContextHolder.getContext().setAuthentication(preAuthenticatedAuthenticationToken);
                    } else if (!SecurityContextHolder.getContext().getAuthentication().getCredentials().equals(token)) {
                        SecurityContextHolder.getContext().setAuthentication(preAuthenticatedAuthenticationToken);
                    }
                }
            }
        } catch (Exception e) {
            ((HttpServletResponse)response).setStatus(HttpServletResponse.SC_FORBIDDEN);
            response.setContentType(APPLICATION_JSON_VALUE);
            objectMapper.writeValue(response.getWriter(), "Error " + e.getMessage());
        }

        chain.doFilter(request, response);
    }

    private String getTokenFromHeader(String header) {

        if (header == null)
            return null;

        if (!header.startsWith(BEARER))
            throw new AuthenticationHeaderException("Invalid authentication scheme found in Authorization header");

        String token = header.substring(BEARER.length());

        if (token.isEmpty())
            throw new AuthenticationHeaderException("Authorization header token is empty");

        return token;
    }
}


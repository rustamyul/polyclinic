package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.MedicalServiceApi;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.MedicalServiceEntity;
import ru.itis.service.MedicalService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class MedicalServiceController implements MedicalServiceApi<MedicalServiceEntity> {

    private final MedicalService medicalService;

    @Override
    public MedicalServiceResponse getMedicalService(UUID id){
        return medicalService.getMedicalService(id);
    }


    @Override
    public PageOfListResponse<MedicalServiceResponse> getMedicalServiceList(int page, int size,
            @Spec(path = "category.id", params = "cat_id", spec = Equal.class)
            Specification<MedicalServiceEntity> spec){
        return medicalService.getMedicalServiceList(page, size, spec);
    }

}

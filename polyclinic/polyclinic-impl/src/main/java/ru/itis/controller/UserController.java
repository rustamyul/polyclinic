package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.UserApi;
import ru.itis.dto.request.CreateUserRequest;
import ru.itis.dto.request.SignInForm;
import ru.itis.dto.response.CreateUserResponse;
import ru.itis.dto.response.TokenCoupleResponse;
import ru.itis.jwt.JwtTokenService;
import ru.itis.service.UserService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
public class UserController implements UserApi {

    private final UserService userService;
    private final JwtTokenService jwtTokenService;

    @Override
    public CreateUserResponse createUser(@Valid CreateUserRequest userRequest) {
        return userService.createUser(userRequest);
    }

    @Override
    public TokenCoupleResponse login(@Valid SignInForm signInForm) {
        return jwtTokenService.generateTokenCouple(userService.login(signInForm));
    }
}

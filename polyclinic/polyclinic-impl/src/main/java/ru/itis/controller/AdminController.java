package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.AdminApi;
import ru.itis.dto.request.EditCategoryRequest;
import ru.itis.dto.request.EditDoctorInfoRequest;
import ru.itis.dto.request.EditMedicalServiceRequest;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.DoctorResponse;
import ru.itis.dto.response.MedicalServiceResponse;
import ru.itis.service.AdminService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('ADMIN')")
public class AdminController implements AdminApi {

    private final AdminService doctorService;
    
    @Override
    public DoctorResponse editDoctorInformation(UUID id, EditDoctorInfoRequest request) {
        return doctorService.editDoctorInformation(id, request);
    }

    @Override
    public DeleteResponse deleteDoctor(UUID id) {
        return doctorService.deleteDoctor(id);
    }

    @Override
    public CategoryResponse editCategory(UUID id, EditCategoryRequest request) {
        return doctorService.editCategory(id, request);
    }

    @Override
    public DeleteResponse deleteCategory(UUID id) {
        return doctorService.deleteCategory(id);
    }

    @Override
    public MedicalServiceResponse editMedicalService(UUID id, EditMedicalServiceRequest request) {
        return doctorService.editMedicalService(id, request);
    }

    @Override
    public DeleteResponse deleteMedicalService(UUID id){
        return doctorService.deleteMedicalService(id);
    }

}

package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.PatientApi;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.service.PatientService;

import java.time.Instant;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
@PreAuthorize("hasAuthority('PATIENT')")
public class PatientController implements PatientApi<UserDetails> {

    private final PatientService patientService;

    @Override
    public PatientAppointmentResponse changeDoctor(UserDetails userDetails, UUID appointmentId, UUID doctorId) {
        return patientService.changeDoctor(userDetails, appointmentId, doctorId);
    }

    @Override
    public PatientAppointmentResponse changeTime(UserDetails userDetails, UUID appointmentId, Instant time) {
        return patientService.changeTime(userDetails, appointmentId, time);
    }
}

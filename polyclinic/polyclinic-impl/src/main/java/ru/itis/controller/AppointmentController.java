package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.GreaterThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.domain.LessThanOrEqual;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.AppointmentApi;
import ru.itis.dto.request.CreateAppointmentRequest;
import ru.itis.dto.request.EditAppointmentRequest;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.dto.response.appointment.DoctorAppointmentResponse;
import ru.itis.dto.response.DeleteResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.dto.response.appointment.PatientAppointmentResponse;
import ru.itis.model.AppointmentEntity;
import ru.itis.service.AppointmentService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class AppointmentController implements AppointmentApi<UserDetails, AppointmentEntity> {

    private final AppointmentService appointmentService;

    @PreAuthorize("hasAuthority('PATIENT')")
    @Override
    public PatientAppointmentResponse createAppointment(UserDetails userDetails, CreateAppointmentRequest createAppointmentRequest) {
        return appointmentService.createAppointment(userDetails, createAppointmentRequest);
    }

    @PreAuthorize("hasAuthority('PATIENT')")
    @Override
    public DeleteResponse cancelAppointment(UserDetails userDetails, UUID id) {
        return appointmentService.cancelAppointment(userDetails, id);
    }

    @PreAuthorize("hasAuthority('PATIENT')")
    @Override
    public FileInfoResponse setFileForAppointment(UserDetails userDetails, UUID id, UUID fileId) {
        return appointmentService.setFileForAppointment(userDetails, id,fileId);
    }

    @PreAuthorize("hasAuthority('DOCTOR')")
    @Override
    public DoctorAppointmentResponse editAppointmentInfo(UserDetails userDetails, UUID id, EditAppointmentRequest request) {
        return appointmentService.editAppointmentInfo(userDetails, id, request);
    }

    @PreAuthorize("hasAuthority('DOCTOR')")
    @Override
    public PageOfListResponse<DoctorAppointmentResponse> getDoctorAppointments(UserDetails userDetails, int page, int size,
                                                                               @And({
                                                                                 @Spec(path = "startTime", params = "from", spec = GreaterThanOrEqual.class),
                                                                                 @Spec(path = "startTime", params = "to", spec = LessThanOrEqual.class)})
                                                                                 Specification<AppointmentEntity> spec) {
        return appointmentService.getDoctorAppointments(userDetails, page, size, spec);
    }

    @PreAuthorize("hasAuthority('PATIENT')")
    @Override
    public PageOfListResponse<PatientAppointmentResponse> getPatientAppointments(UserDetails userDetails, int page, int size,
                                                                               @And({
                                                                                 @Spec(path = "startTime", params = "from", spec = GreaterThanOrEqual.class),
                                                                                 @Spec(path = "startTime", params = "to", spec = LessThanOrEqual.class)})
                                                                                 Specification<AppointmentEntity> spec) {
        return appointmentService.getPatientAppointments(userDetails, page, size, spec);
    }


}

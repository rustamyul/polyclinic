package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.Equal;
import net.kaczmarzyk.spring.data.jpa.domain.In;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Or;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.DoctorApi;
import ru.itis.dto.request.CreateDoctorRequest;
import ru.itis.dto.response.*;
import ru.itis.model.DoctorEntity;
import ru.itis.service.DoctorService;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class DoctorController implements DoctorApi<UserDetails, DoctorEntity> {

    private final DoctorService doctorService;

    @PreAuthorize("hasAuthority('DOCTOR')")
    @Override
    public DoctorResponse createDoctor(UserDetails userDetails, @Valid CreateDoctorRequest request) {
        return doctorService.createDoctor(userDetails, request);
    }

    @PreAuthorize("hasAuthority('DOCTOR')")
    @Override
    public FileInfoResponse setDoctorPhoto(UserDetails userDetails, UUID photoId) {
        return doctorService.setDoctorPhoto(userDetails, photoId);
    }

    @Override
    public DoctorResponse getDoctorById(UUID id) {
        return doctorService.getDoctorById(id);
    }

    @Override
    public PageOfListResponse<DoctorResponse> getDoctors(int page, int size,
             @Join(path = "medicalServices", alias = "ms")
             @Join(path = "ms.category", alias = "c")
             @Or({
                     @Spec(path = "ms.id", params = "med_ser_id", spec = In.class),
                     @Spec(path = "c.id", params = "cat_id", spec = Equal.class)
             }) Specification<DoctorEntity> spec) {
        return doctorService.getDoctors(page, size, spec);
    }
}

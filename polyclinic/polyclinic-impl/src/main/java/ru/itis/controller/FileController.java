package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ContentDisposition;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import ru.itis.api.FileApi;
import ru.itis.dto.response.ContentResponse;
import ru.itis.dto.response.FileInfoResponse;
import ru.itis.service.FileService;

import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;

@RestController
@RequiredArgsConstructor
public class FileController implements FileApi {

    private final FileService fileService;

    @Override
    public FileInfoResponse getFileInfo(UUID uuid) {
        return fileService.getFileInfo(uuid);
    }

    @Override
    public ResponseEntity<byte[]> getFile(UUID uuid) {
        ContentResponse contentResponse = fileService.getFile(uuid);

        ContentDisposition contentDisposition = ContentDisposition.builder("attachment")
                .filename(contentResponse.getFilename(), UTF_8)
                .build();

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentResponse.getContentType()))
                .header(CONTENT_DISPOSITION, contentDisposition.toString())
                .body(contentResponse.getFile());
    }

    @Override
    public FileInfoResponse saveFile(MultipartFile multipartFile) {
        return fileService.saveFile(multipartFile);
    }
}

package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.CategoryApi;
import ru.itis.dto.response.CategoryResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.service.CategoryService;

import java.util.UUID;

@RestController
@RequiredArgsConstructor
public class CategoryController implements CategoryApi {

    private final CategoryService categoryService;

    @Override
    public PageOfListResponse<CategoryResponse> getCategoryList(int page, int size) {
        return categoryService.getCategoryList(page, size);
    }

    @Override
    public CategoryResponse getCategoryById(UUID id) {
        return categoryService.getCategoryById(id);
    }
}

package ru.itis.controller;

import lombok.RequiredArgsConstructor;
import net.kaczmarzyk.spring.data.jpa.domain.*;
import net.kaczmarzyk.spring.data.jpa.web.annotation.And;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Join;
import net.kaczmarzyk.spring.data.jpa.web.annotation.Spec;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.web.bind.annotation.RestController;
import ru.itis.api.FreeAppointmentApi;
import ru.itis.dto.response.appointment.FreeAppointmentResponse;
import ru.itis.dto.response.PageOfListResponse;
import ru.itis.model.FreeAppointmentEntity;
import ru.itis.service.FreeAppointmentService;

@RestController
@RequiredArgsConstructor
public class FreeAppointmentController implements FreeAppointmentApi<FreeAppointmentEntity>{

    private final FreeAppointmentService freeAppointmentService;

    @Override
    public PageOfListResponse<FreeAppointmentResponse> getFreeAppointments(int page, int size,
          @Join(path = "doctor", alias = "d")
          @Join(path = "d.medicalServiceEntities", alias = "ms")
          @Join(path = "ms.category", alias = "c")
          @And({
                  @Spec(path = "state", constVal = "FREE", spec = Equal.class),
                  @Spec(path = "d.id", params = "doctor_id", spec = Equal.class),
                  @Spec(path = "ms.id", params = "med_ser_id", spec = Equal.class),
                  @Spec(path = "c.id", params = "cat_id", spec = Equal.class),
                  @Spec(path = "startTime", params = "from", spec = GreaterThanOrEqual.class),
                  @Spec(path = "startTime", params = "to", spec = LessThanOrEqual.class)
          }) Specification<FreeAppointmentEntity> spec){
       return freeAppointmentService.getFreeAppointments(page, size, spec);
    }
}

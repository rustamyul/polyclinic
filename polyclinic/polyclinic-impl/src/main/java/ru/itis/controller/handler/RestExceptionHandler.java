package ru.itis.controller.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.itis.dto.response.ExceptionResponse;
import ru.itis.validation.dto.ValidationErrorResponse;
import ru.itis.validation.dto.ValidationExceptionResponse;
import ru.itis.exception.ApplicationServiceException;

import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@ControllerAdvice
public class RestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ValidationExceptionResponse> handleExceptions(MethodArgumentNotValidException exception){
        log.warn("Got validation exception " + exception.getMessage() + ", status: " + HttpStatus.BAD_REQUEST);
        List<ValidationErrorResponse> errors = new ArrayList<>();
        exception.getBindingResult().getAllErrors().forEach((error) -> {

            String errorMessage = error.getDefaultMessage();
            ValidationErrorResponse errorDto = ValidationErrorResponse.builder()
                    .message(errorMessage)
                    .build();

            if (error instanceof FieldError) {
                String fieldName = ((FieldError) error).getField();
                errorDto.setField(fieldName);
            } else {
                String objectName = error.getObjectName();
                errorDto.setObject(objectName);
            }
            errors.add(errorDto);
        });
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(ValidationExceptionResponse.builder()
                .errors(errors)
                .build());
    }

    @ExceptionHandler(ApplicationServiceException.class)
    public final ResponseEntity<ExceptionResponse> handleExceptions(ApplicationServiceException exception) {
        log.error("Got exception " + exception.getMessage() + ", status: " + exception.getHttpStatus());
        return wrapException(exception, exception.getHttpStatus());
    }

    @ExceptionHandler(AccessDeniedException.class)
    public final ResponseEntity<ExceptionResponse> handleExceptions(AccessDeniedException exception) {
        log.error("Got exception " + exception.getMessage() + ", status: " + HttpStatus.FORBIDDEN);
        return wrapException(exception, HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ExceptionResponse> handleExceptions(Exception exception) {
        log.error("Got exception " + exception.getMessage());
        return wrapException(exception, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<ExceptionResponse> wrapException(Exception exception, HttpStatus status) {
        return ResponseEntity.status(status)
                .body(ExceptionResponse.builder()
                        .message(exception.getMessage())
                        .exceptionName(exception.getClass().getSimpleName())
                        .build());
    }

}
